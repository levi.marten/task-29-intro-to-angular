import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'task29-angular-intro';
  
  public shoutOut: string = "Shout-out to all the dev teachers out there making us learn new cool things!";
  
  public answer: number = 42;

  public shoppingList: string[] = ['Candy🍬', 'Donuts🍩', 'Ice cream🍦', 'Frozen pizzas🍕', 'coffee☕', 'beer🍻', 'tp🧻🧻🧻'];


  public bad: string = 'Storing passwords in plain view is bad practice, but it makes good example for how to use Mustache syntax and Structural Directives in Angular.';

  public dataBase: any = {
    id: 1,
    username: 'AzureDiamond',
    password: 'hunter2'
  };

  public moves = 'https://www.youtube.com/watch?v=GvG7CHWyol0';

};